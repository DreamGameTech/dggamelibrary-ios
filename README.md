---
---
---
*---   简体中文    ---*
--

## DGGameLibray-iOS 是DreamGame Technology 公司游戏产品的SDK，供平台商对接我司游戏使用

## 该SDK包含DG的游戏为： *百家乐、龙虎、牛牛、轮盘、骰宝*

## 请使用真机调试，不支持虚拟器<br>   支持iOS 9以上，iPhone、iPad的armv7、arm64架构

### 文档修改日志

版本号 | 修改内容 | 修改日期
----|------|----
2.1.1 | 优化聊天表情| 2021-01-07
2.1.0 | iPhone 12系列 兼容| 2020-11-26
2.0.9 | 视频加密| 2020-08-29
2.0.8 | 添加隐藏聊天功能<br> [DGSDKHANDLE DGSDKExistBarrage];| 2020-04-06
2.0.7 | 隐藏加载失败Toast| 2020-01-16
2.0.6 | 兼容新版聊天功能| 2020-01-03
2.0.5 | 资源文件解压失败回调| 2019-12-09
2.0.4 | 更新资源文件路径| 2019-12-07
2.0.3 | iPhone 11系列 兼容| 2019-10-25
2.0.2 | 1.登录接口添加配置文件传入，对接登录API，返回配置文件字符串，传入SDK。<br> 2.添加轮盘、骰宝游戏。<br> 3.SDK内存优化，减少10.3M。   | 2019-10-23
2.0.0 | HttpDNS已内置， 工程可删除HttpDNS库  | 2019-10-15
1.9.0 | 登录接口添加最小下注，minBet:最小下注额度，0不限制  | 2019-09-11

###  常见错误

#### 1.假设cocos项目 Other Linker Flags  不能使用-ObjC库的话,  使用-force_load
```
bug show：
+[DGLoginBean DGmj_objectWithKeyValues:]: unrecognized selector sent to class 0x106a0c358
```

  
  
### 对接方式
    
#### 1. 添加配置
(1) TARGETS：
             framework使用视频框架，需引入系统框架<b>VideoToolbox.framework</b>  


(2) info.plist ：
             支持HTTP的请求
             
```
App Transport Security Settings     (Type:Dictionary)   
-> Allow Arbitrary Loads (Type:Boolean Value:YES)
```

(3) Build Settings ：
             由于framework的配置， 需要添加配置
             
```
Linking 
    >Other Linker Flags    添加： -ObjC
    
添加库
TARGETS->build Phases->Link Binary With Libraries  添加库Libresolv    
```      

### 2.引入SDK
相关文件在DGSDK 目录内，请前往查看。    
需要把DGSDK.bundle、DGSDKResources.bundle、DGSDK.framework 拖入工程。   
<b>备注DGSDK.bundle 在DGSDK.framework内</b>

```
DGSDK.bundle
DGSDKResources.bundle
DGSDK.framework 


----- 2.0版本以上， httpDNS已内置， 可删除httpDNS库 -----
最新引入HttpDNS ， 整个HttpDns文件夹 拖入工程，假如提示失败，请到工程配置查看路径是否正确，
Seaerch Paths -> Framework Search Paths

AlicloudHttpDNS.framework 
UTMini.framework 
AlicloudUtils.framework 
AlicloudBeacon.framework 
UTDID.framework 
--------------------------------------------
```     
#### 3. 代码接入SDK

(1) 引入SDK文件：

```
#import <DGSDK/DGSDKHandle.h>  
#import <DGSDK/DGGameId.h>  
```

(2) 监听SDK消息通知    <b>---可选</b>
> DGSDKWILLENTERGAME：即将进入SDK   
 DGSDKENTERGAME：已经进入SDK    
 DGSDKEXITEDGAME  ：已经退出SDK    
 DGSDKENTEREDGAMEFAIL   ：进入SDK失败 -- 有值回调
 
> 数据加载失败的回调，i 有三个类型   
> key:DGError  
> 0 --- socket链接失败   
> 1 --- 会员初始化失败   
> 2 --- 会员账号暂停   
> 3 --- 资源加载失败	   
	



(3) 调用SDK
	
> token:(String)                  需要登录用户token  
VC:(UIViewControler)     当前的ViewController  
gameID:(Int)                    传游戏ID    ID表详见DGGameId.h文案  
isLoadDGGame:(Bool)    显示SDK加载Toast    YES显示， False不显示  
 minBet:(Int)                      余额最小下注额度，小于设定额度提示充值，0不限制  
domains:(String)          获取配置文件的字符串， 对接登录api会提供





> gameID注释   
DGBACCARAT： 百家乐    
DGDRAGON：  龙虎   
DGBULL：    牛牛   
DGROULETTE：    轮盘  
DGSICBO：    骰宝   
 
```
- (void)DGSDKLoginWithToken:(NSString *)token
         withViewController:(UIViewController *)VC
                 withGameID:(int)gameID
      withLoadDGGameLoading:(BOOL)isShowHUD
                 withMinBet:(int)minBet
                   withData:(NSString *)domains;
```

代码演示

```
  [DGSDKHANDLE DGSDKLoginWithToken:token
                withViewController:self
                        withGameID:DGBACCARAT
             withLoadDGGameLoading:YES 
                        withMinBet:50
                          withData:dataConfig];

```

---
---
---
*---   English    ---*
--

## DGGameLibray-iOS is the SDK product of DreamGame Technology, allowing merchant or operator to integrate with our product for usage. 



## The SDK product includes DreamGaming<br> *Baccarat, DragonTiger, Bull Bull, Roulette and SicBo*


## Please test it with Actual Machine, Does not support Virtual Machine (VM)<br>  Can be support by IOS9 and above, Iphone, Ipad and armv7,arm64 structure

### Change Log

VersionNumber | Updated Content | Updated DateTime
----|------|----
2.1.1 | Optimize chat emoticons| 2021-01-07
2.1.0 | iPhone 12 Compatible| 2020-11-26
2.0.9 | Video Encryption| 2020-08-29
2.0.8 | Add hidden Chat Function<br> [DGSDKHANDLE DGSDKExistBarrage];| 2020-04-06
2.0.7 | Hidden Load Fail Toast| 2020-01-16
2.0.6 | Compatible new Version Chat Function| 2020-01-03
2.0.5 | Resource file decompression failure callback| 2019-12-09
2.0.4 | Update resource file path| 2019-12-07
2.0.3 | iPhone 11 Compatible| 2019-10-25
2.0.2 | 1. Add a configuration file to the login interface, connect to the login API, return the configuration file string, and pass it to the SDK. <br> 2. Add roulette and Sic Bo games. <br> 3.SDK memory optimization, reduce 10.3M。   | 2019-10-23
2.0.0 | HttpDNS is built in, the project can delete the HttpDNS library  | 2019-10-15
1.9.0 | Login interface to add minimum bet, minBet: minimum bet amount, 0 unlimited  | 2019-09-11


###  Common mistakes

#### 1.If the cocos project Other Linker Flags cannot use the -ObjC library, use -force_load
```
bug show：
+[DGLoginBean DGmj_objectWithKeyValues:]: unrecognized selector sent to class 0x106a0c358
```

  
  
### Integration Method
    
#### 1. Add configuration
(1) TARGETS：
            Due to the configuration of the framework, you need to add configuration<br><b>VideoToolbox.framework</b>  


(2) info.plist ：
             Support HTTP request
             
```
App Transport Security Settings     (Type:Dictionary)   
-> Allow Arbitrary Loads (Type:Boolean Value:YES)
```

(3) Build Settings ：
             Due to the configuration of the framework, you need to add configuration
             
```
Linking 
    >Other Linker Flags    Add ： -ObjC
    
Import SDK files
TARGETS->build Phases->Link Binary With Libraries  add:Libresolv    
```      

### 2.Import SDK files
The relevant files are in the DGSDK directory, kindly check.    
Need to import DGSDK.bundle、DGSDKResources.bundle、DGSDK.framework to the project.
<b>Notice: bundle included into framework</b>

```
DGSDK.bundle
DGSDKResources.bundle
DGSDK.framework 


----- 2.0 version and above, httpNDS installed, httpDNS library can be delete -----
The latest import httpDNS, drag the entire httpDNS into program, if failure occur, please proceed to program configuration to reconfirm，
Seaerch Paths -> Framework Search Paths

AlicloudHttpDNS.framework 
UTMini.framework 
AlicloudUtils.framework 
AlicloudBeacon.framework 
UTDID.framework 
--------------------------------------------
```     
#### 3. Code access SDK

(1) Import SDK files：

```
#import <DGSDK/DGSDKHandle.h>  
#import <DGSDK/DGGameId.h>  
```

(2) Monitor SDK notification  ---Optional
> DGSDKWILLENTERGAME：Going to enter SDK   
 DGSDKENTERGAME：Entered SDK    
 DGSDKEXITEDGAME  ：Exited SDK    
 DGSDKENTEREDGAMEFAIL   ：Enter SDK Failure --Callback value
 
> Callback of data downloading failure, "I" have 3 types   
> key:DGError  
> 0 --- socket connection failure  
> 1 --- Member initialization failure  
> 2 --- Member account suspended  
> 3 --- Sources uploading failure	 
	



(3) Transfer SDK
	
> token:(String)                  Member login needed Token  
VC:(UIViewControler)     Current ViewController  
gameID:(Int)                    Game ID transfer, ID table looking into DGGameid.h detail  
isLoadDGGame:(Bool)    Show SDK uploaded Toast, YES for Display, Failed for Not Display  
 minBet:(Int)                      Leftover minimum bet, less than minimum bet setting show Reload, 0 not restricted 
domains:(String)          Obtain String of configuration files, integrate Login API will be provided





> gameID Detail   
DGBACCARAT： Baccarat    
DGDRAGON：  Dragon   
DGBULL：    Bull   
DGROULETTE：    Roulette  
DGSICBO：    Sicbo   
 
```
- (void)DGSDKLoginWithToken:(NSString *)token
         withViewController:(UIViewController *)VC
                 withGameID:(int)gameID
      withLoadDGGameLoading:(BOOL)isShowHUD
                 withMinBet:(int)minBet
                   withData:(NSString *)domains;
```

Code Sample

```
  [DGSDKHANDLE DGSDKLoginWithToken:token
                withViewController:self
                        withGameID:DGBACCARAT
             withLoadDGGameLoading:YES 
                        withMinBet:50
                          withData:dataConfig];

```
